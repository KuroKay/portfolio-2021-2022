import { Canvas, useFrame, useLoader, useThree } from '@react-three/fiber'
import CameraControls from 'camera-controls'
import randomColor from 'randomcolor'
import { Suspense, useMemo, useRef, useState } from 'react'
import * as THREE from 'three'
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader"


CameraControls.install({ THREE })
const randomPos = (min = 5, max = -5) => Math.random() * (max - min) + min

/* Camera */
const Controls = ({ zoom, focus, pos = new THREE.Vector3(), look = new THREE.Vector3() }) => {
  const camera = useThree((state) => state.camera)
  const gl = useThree((state) => state.gl)
  const controls = useMemo(() => new CameraControls(camera, gl.domElement), [])
  return useFrame((state, delta) => {
    zoom ? pos.set(focus.x , focus.y, focus.z + 0.2) : pos.set(0, 0.5, 7)
    zoom ? look.set(focus.x, focus.y, focus.z - 0.2) : look.set(0, 0, 4)

    state.camera.position.lerp(pos, 0.5)
    state.camera.updateProjectionMatrix()

    controls.setLookAt(state.camera.position.x , state.camera.position.y, state.camera.position.z, look.x, look.y, look.z, true)
    
    return controls.update(delta)
  })
}

/* Elements */
const Cloud = ({ momentsData, zoomToView })  => {
  return momentsData.map(({ position, color }, i) => (
    <mesh key={i} position={position} onClick={(e) => zoomToView(e.object.position)}>
      <boxGeometry args={[0.1, 0.08, 0.003]} />
      <meshStandardMaterial color={color} />
    </mesh>
  ))
}

const momentsArray = [
  {
    color: randomColor(),
    position: [0, 0, 4],
  },
  {
    color: randomColor(),
    position: [1, 0, 4],
  },
  {
    color: randomColor(),
    position: [-1, 0, 4],
  },
  {
    color: randomColor(),
    position: [-1.5, 0, 4],
  },
]

/* Planet art */
const PlanetArt = () => {
  const gltf = useLoader(GLTFLoader, "./models/planete-art.gltf");
  const planetArt = useRef()
  return (
    <mesh ref={planetArt} position={[1.25, 0.75, 4]} rotation={[0, -1.9, -0.2]}>
      <primitive object={gltf.scene} scale={0.2} />
    </mesh>
  );
};

/* Planet Code */
const PlanetCode = () => {
  const planetCodeGLTF = useLoader(GLTFLoader, "./models/planete-code.gltf");
  const planetCode = useRef()
  return (
    <mesh ref={planetCode} position={[1.5, -0.25, 5.5]} rotation={[0, -2.5, 0]}>
      <primitive object={planetCodeGLTF.scene} scale={0.2} />
    </mesh>
  );
};

/* Planet Cooking */
const PlanetCooking = () => {
  const planetCookingGLTF = useLoader(GLTFLoader, "./models/planete-cooking.gltf");
  const planetCooking = useRef()
  return (
    <mesh ref={planetCooking} position={[-2, 0.5, 4]} rotation={[0, -2.5, 0]}>
      <primitive object={planetCookingGLTF.scene} scale={0.2} />
    </mesh>
  );
};

/* Planet Movie */
const PlanetMovie = () => {
  const planetMovieGLTF = useLoader(GLTFLoader, "./models/planete-movie.gltf");
  const planetMovie = useRef()
  return (
    <mesh ref={planetMovie} position={[-2.3, -1, 5]} rotation={[0, -0.5, 0]}>
      <primitive object={planetMovieGLTF.scene} scale={0.2} />
    </mesh>
  );
};


/* Scene with all 3d elements */
const Scene = (): JSX.Element => {
  const [zoom, setZoom] = useState(false);
  const [focus, setFocus] = useState({});

  return (
    <div className="main">
      <button type="button" onClick={ () => (setZoom(!zoom), setFocus({x: 0.3, y: 0, z: 4.2})) }>Gooo !</button>
      <button type="button" onClick={ () => (setZoom(!zoom), setFocus({ })) }>Left</button>
      <Canvas
        style={{ height: '100vh', width: '100vw' }}
        linear
        camera={{ position: [0, 0, 5], }}
      >
        <ambientLight />
        <directionalLight position={[150, 150, 150]} intensity={0.55} />
        {/*
        <Cloud
          momentsData={momentsArray}
          zoomToView={(focusRef) => (setZoom(!zoom), setFocus(focusRef))}
        />
        */}
        <Controls zoom={zoom} focus={focus} />
        <Suspense fallback={null}>
          <PlanetArt />
          <PlanetCode />
          <PlanetCooking />
          <PlanetMovie />
        </Suspense>
      </Canvas>
      <button type="button">Left</button>
    </div>
  );
};
export default Scene;
